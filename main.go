package main

import (
	"log"
	"orderTransfer/internal"
)

func main() {
	s, err := internal.NewSvc()
	if err != nil {
		log.Fatal(err)
	}

	s.StartCron()
	s.StartServer()
}
