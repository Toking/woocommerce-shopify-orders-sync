package internal

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func (s *Svc) StartServer() {
	addr, err := determineListenAddress()
	if err != nil {
		log.Fatal(err)
	}
	http.HandleFunc("/ping", func(writer http.ResponseWriter, request *http.Request) {
		log.Println("pinged")
	})
	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return ":8080", nil
	}
	return ":" + port, nil
}

func PingService() {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "yourUrl/ping", nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Close = true

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println(string(bodyBytes))
		fmt.Println(err)
	}
	return
}
