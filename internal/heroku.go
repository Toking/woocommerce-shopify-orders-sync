package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const herokuToken = "465425af-92a0-40ed-9bc5-78f4956b5dee"

type HerokuConfig struct {
	Timestamp string `json:"timestamp"`
}

func GetConfigVars() (map[string]string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.heroku.com/apps/send-to-app/config-vars", nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", "Bearer "+herokuToken)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/vnd.heroku+json; version=3")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	var res map[string]string
	json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func UpdateConfigVars(timestamp string) (*HerokuConfig, error) {
	client := &http.Client{}
	vars := &HerokuConfig{
		Timestamp: timestamp,
	}
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(vars)
	req, err := http.NewRequest("PATCH", "https://api.heroku.com/apps/send-to-app/config-vars", b)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", "Bearer "+herokuToken)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/vnd.heroku+json; version=3")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	res := &HerokuConfig{}
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
