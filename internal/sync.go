package internal

import (
	"fmt"
	"github.com/samber/lo"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"
)

func (s *Svc) StartSync() {
	log.Print("Sync is started")
	configVars, err := GetConfigVars()
	if err != nil {
		log.Print(configVars)
		return
	}

	shopifyData := make(map[string]string)
	for key, value := range configVars {
		if key == "timestamp" {
			s.Timestamp = value
		} else {
			shopifyData[key] = value
		}
	}
	s.Config.Shopify = shopifyData

	if s.Timestamp == "" {
		s.Timestamp = time.Now().Add(-64 * time.Hour).Format(time.RFC3339)
	}
	currentTimestamp := time.Now().Format(time.RFC3339)

	s.syncShopifyOrdersToWoo()
	s.syncWooOrdersToShopify()

	_, err = UpdateConfigVars(currentTimestamp)
	if err != nil {
		log.Print(configVars)
		return
	}
	log.Print("Sync is finished")
}

func (s *Svc) syncShopifyOrdersToWoo() {
	var (
		wg = sync.WaitGroup{}
	)

	//data from shopify to woo
	for webshop, token := range s.Config.Shopify {
		wg.Add(1)
		go s.syncClientToWoo(&wg, webshop, token)
	}
	wg.Wait()
}

func (s *Svc) syncClientToWoo(wg *sync.WaitGroup, webshop, token string) {
	defer wg.Done()
	shopifyCli := NewShopifyClient(webshop, token)

	var orders []ShopifyOrder
	sinceID := 0
	for {
		results, err := s.GetShopifyUpdatedOrders(shopifyCli, sinceID)
		if err != nil {
			log.Print(err)
			return
		}

		//if read all records from shopify, break the for loop
		if len(results) == 0 {
			break
		}

		sinceID = results[len(results)-1].ID
		orders = append(orders, results...)
	}

	log.Print(fmt.Sprintf("Got %d updated orders from shopify for %s", len(orders), webshop))

	for _, o := range orders {
		s.syncOrderToWoo(o)
	}
}

func (s *Svc) syncOrderToWoo(order ShopifyOrder) {
	//skip orders without wooID in notes or fulfillment
	if !strings.Contains(order.Note, "SendTo_") || len(order.Fulfillments) < 1 {
		return
	}

	wooID := strings.TrimPrefix(order.Note, "SendTo_")
	trackingNumber := order.Fulfillments[0]["tracking_number"].(string)
	createdAt := order.Fulfillments[0]["created_at"].(string)

	var metaData []MetaData
	trackingData := MetaData{
		Key:   "shopify_tracking_number",
		Value: trackingNumber,
	}
	dateOfDispatch := MetaData{
		Key:   "shopify_date_of_dispatch",
		Value: createdAt,
	}

	metaData = append(metaData, trackingData, dateOfDispatch)
	_, err := s.WooCli.UpdateWooOrderWithShopifyID(wooID, UpdateWooOrder{"processing", metaData})
	if err != nil {
		log.Print(err)
		return
	}
}

func (s *Svc) syncWooOrdersToShopify() {
	var (
		orders []WooOrder
		wg     = sync.WaitGroup{}
	)
	page := 1
	for {
		results, err := s.WooCli.GetWooOrdersByTimestamp(s.Timestamp, page, 50)
		if err != nil {
			log.Print(err)
			return
		}

		orders = append(orders, results...)
		if len(results) < 50 {
			break
		}
		page++
	}

	//data from woo to shopify
	for webshop, token := range s.Config.Shopify {
		wg.Add(1)
		go s.syncClientToShopify(&wg, orders, webshop, token)
	}
	wg.Wait()
}

func (s *Svc) syncClientToShopify(wg *sync.WaitGroup, orders []WooOrder, webshop, token string) {
	defer wg.Done()

	shopifyCli := NewShopifyClient(webshop, token)
	for _, o := range orders {
		if len(o.LineItems) < 1 {
			continue
		}

		meta, ok := lo.Find[MetaData](o.LineItems[0].MetaData, func(m MetaData) bool {
			return m.Key == "shopify_webshop"
		})

		orderStatus, okStatus := lo.Find[MetaData](o.MetaData, func(m MetaData) bool {
			return m.Key == "_order_user_status"
		})

		if !ok || fmt.Sprintf("%v", meta.Value) != webshop {
			continue
		}

		if !okStatus || fmt.Sprintf("%v", orderStatus.Value) != "Confirm" {
			continue
		}

		shopifyOrder, err := s.syncOrderToShopify(o, shopifyCli)
		if err != nil {
			log.Print(err)
			continue
		}

		if shopifyOrder == nil {
			continue
		}

		//update wooOrder with shopify orderID
		metada := o.MetaData
		metada = append(metada, MetaData{Key: "shopify_order_id", Value: strconv.Itoa(shopifyOrder.ID)})

		_, err = s.WooCli.UpdateWooOrderWithShopifyID(strconv.Itoa(o.Id), UpdateWooOrder{MetaData: metada})
		if err != nil {
			log.Print(err)
			continue
		}
	}
}

func (s *Svc) syncOrderToShopify(order WooOrder, shopifyCli *ShopifyClient) (*ShopifyOrder, error) {
	//skip orders with shopifyOrder metadata
	val, ok := lo.Find[MetaData](order.MetaData, func(m MetaData) bool {
		return m.Key == "shopify_order_id"
	})

	if ok && fmt.Sprintf("%v", val.Value) != "" {
		return nil, nil
	}

	var err error
	shopifyOrder := s.WooToShopifyOrder(order)

	shopifyOrder, err = s.CreateShopifyOrder(shopifyOrder, shopifyCli)
	if err != nil {
		return nil, err
	}

	return shopifyOrder, nil
}
