package internal

type (
	ShopifyOrder struct {
		Domain                string      `json:"-"`
		ID                    int         `json:"id,omitempty"`
		Email                 string      `json:"email,omitempty"`
		Number                int         `json:"number,omitempty"`
		Token                 string      `json:"token,omitempty"`
		Gateway               string      `json:"gateway,omitempty"`
		Test                  bool        `json:"test,omitempty"`
		Note                  string      `json:"note,omitempty"`
		TotalPrice            string      `json:"total_price,omitempty"`
		SubtotalPrice         string      `json:"subtotal_price,omitempty"`
		TotalWeight           int         `json:"total_weight,omitempty"`
		TotalTax              string      `json:"total_tax,omitempty"`
		TaxesIncluded         bool        `json:"taxes_included,omitempty"`
		Currency              string      `json:"currency,omitempty"`
		FinancialStatus       string      `json:"financial_status,omitempty"`
		Confirmed             bool        `json:"confirmed,omitempty"`
		TotalDiscounts        string      `json:"total_discounts,omitempty"`
		TotalLineItemsPrice   string      `json:"total_line_items_price,omitempty"`
		BuyerAcceptsMarketing bool        `json:"buyer_accepts_marketing,omitempty"`
		Name                  string      `json:"name,omitempty"`
		TotalPriceUsd         string      `json:"total_price_usd,omitempty"`
		UserID                int64       `json:"user_id,omitempty"`
		Phone                 string      `json:"phone,omitempty"`
		AppID                 int         `json:"app_id,omitempty"`
		OrderNumber           int         `json:"order_number,omitempty"`
		PaymentGatewayNames   []string    `json:"payment_gateway_names,omitempty"`
		ProcessingMethod      string      `json:"processing_method,omitempty"`
		FulfillmentStatus     interface{} `json:"fulfillment_status,omitempty"`
		TaxLines              []struct {
			Price string  `json:"price"`
			Rate  float64 `json:"rate"`
			Title string  `json:"title"`
		} `json:"tax_lines"`
		LineItems       []ShopifyLineItem        `json:"line_items"`
		Fulfillments    []map[string]interface{} `json:"fulfillments"`
		ShippingLines   []ShopifyShippingLine    `json:"shipping_lines"`
		Customer        Customer                 `json:"customer"`
		ShippingAddress ShippingAddress          `json:"shipping_address"`
	}

	Customer struct {
		FirstName string `json:"first_name,omitempty"`
		Email     string `json:"email,omitempty"`
	}

	ShopifyShippingLine struct {
		Code  string `json:"code"`
		Price string `json:"price"`
		Title string `json:"title"`
	}

	ShippingAddress struct {
		Address1     string      `json:"address1,omitempty"`
		Address2     string      `json:"address2,omitempty"`
		City         string      `json:"city,omitempty"`
		Company      interface{} `json:"company,omitempty"`
		Country      string      `json:"country,omitempty"`
		FirstName    string      `json:"first_name,omitempty"`
		LastName     string      `json:"last_name,omitempty"`
		Phone        string      `json:"phone,omitempty"`
		Province     string      `json:"province,omitempty"`
		Zip          string      `json:"zip,omitempty"`
		Name         string      `json:"name,omitempty"`
		CountryCode  string      `json:"country_code,omitempty"`
		ProvinceCode string      `json:"province_code,omitempty"`
	}

	ShopifyLineItem struct {
		ID                         int         `json:"id,omitempty"`
		VariantID                  int         `json:"variant_id,omitempty"`
		Title                      string      `json:"title,omitempty"`
		Quantity                   int         `json:"quantity,omitempty"`
		Sku                        string      `json:"sku,omitempty"`
		Vendor                     string      `json:"vendor,omitempty"`
		FulfillmentService         string      `json:"fulfillment_service,omitempty"`
		ProductID                  int         `json:"product_id,omitempty"`
		RequiresShipping           bool        `json:"requires_shipping,omitempty"`
		Taxable                    bool        `json:"taxable,omitempty"`
		GiftCard                   bool        `json:"gift_card,omitempty"`
		Name                       string      `json:"name,omitempty"`
		VariantInventoryManagement string      `json:"variant_inventory_management,omitempty"`
		ProductExists              bool        `json:"product_exists,omitempty"`
		FulfillableQuantity        int         `json:"fulfillable_quantity,omitempty"`
		Grams                      int         `json:"grams,omitempty"`
		Price                      string      `json:"price,omitempty"`
		TotalDiscount              string      `json:"total_discount,omitempty"`
		FulfillmentStatus          interface{} `json:"fulfillment_status,omitempty"`
	}

	CreateOrderRequest struct {
		Order *ShopifyOrder `json:"order"`
	}

	GetOrdersRes struct {
		Orders []ShopifyOrder `json:"orders"`
	}
)
