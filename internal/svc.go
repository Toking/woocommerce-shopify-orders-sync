package internal

type (
	Svc struct {
		Config *Config
		WooCli *WooClient
		Timestamp string
	}
)

func NewSvc() (*Svc, error) {
	conf, err := New("config.json")
	if err != nil {
		return nil, err
	}

	wooCli := NewWooClient(conf)

	s := &Svc{
		Config: conf,
		WooCli: wooCli,
	}

	return s, nil
}
