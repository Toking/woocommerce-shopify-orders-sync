package internal

import (
	"github.com/robfig/cron/v3"
	"log"
	"os"
)

func (s *Svc) StartCron()  {
	logger := cron.VerbosePrintfLogger(log.New(os.Stdout, "", log.LstdFlags))
	cronJob := cron.New(
		cron.WithSeconds(),
		cron.WithChain(
			cron.Recover(logger),
			cron.SkipIfStillRunning(logger),
		),
	)

	cronJob.AddFunc("@every 15m", func() {
		s.StartSync()
	})

	cronJob.AddFunc("@every 15m", func() {
		PingService()
	})

	cronJob.Start()
}
