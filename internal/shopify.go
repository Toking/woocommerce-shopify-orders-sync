package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type (
	ShopifyClient struct {
		WebshopUrl string
		BaseUrl    string
		Token      string
		Cli        *http.Client
	}
)

func NewShopifyClient(webshop, token string) *ShopifyClient {
	return &ShopifyClient{
		webshop,
		fmt.Sprintf("https://%s/admin/api/2022-07", webshop),
		token,
		http.DefaultClient,
	}
}

func (s *Svc) GetShopifyUpdatedOrders(cli *ShopifyClient, sinceID int) ([]ShopifyOrder, error) {
	path := cli.BaseUrl + "/orders.json"

	q := url.Values{}
	q.Add("limit", "50")
	q.Add("since_id", strconv.Itoa(sinceID))
	q.Add("updated_at_min", s.Timestamp)
	q.Add("status", "any")

	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, err
	}
	req.URL.RawQuery = q.Encode()

	req.Header.Add("Host", cli.WebshopUrl)
	req.Header.Add("Accept", "*/*")
	req.Header.Add("X-Shopify-Access-Token", cli.Token)

	resp, err := cli.Cli.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	res := &GetOrdersRes{}
	if err := json.NewDecoder(bytes.NewBuffer(bodyBytes)).Decode(&res); err != nil {
		return nil, err
	}

	if len(res.Orders) < 1 {
		return nil, nil
	}

	return res.Orders, nil
}

func (s *Svc) CreateShopifyOrder(order *ShopifyOrder, cli *ShopifyClient) (*ShopifyOrder, error) {
	path := cli.BaseUrl + "/orders.json"
	body := &CreateOrderRequest{
		Order: order,
	}

	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, path, bytes.NewReader(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Host", cli.WebshopUrl)
	req.Header.Add("Accept", "*/*")
	req.Header.Add("X-Shopify-Access-Token", cli.Token)
	req.Header.Add("Content-Length", strconv.Itoa(len(jsonBody)))
	req.Header.Add("Content-Type", "application/json")

	resp, err := cli.Cli.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	res := &CreateOrderRequest{}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("create order request failed with status code %d, body: %s", resp.StatusCode, string(bodyBytes))
	}
	if err := json.NewDecoder(bytes.NewBuffer(bodyBytes)).Decode(&res); err != nil {
		return nil, err
	}

	return res.Order, nil
}

func (s *Svc) WooToShopifyOrder(order WooOrder) *ShopifyOrder {
	var (
		firstName, lastName, address1, city, zip, phone, cliFirstname, cliLastName string
	)

	firstName = "Firstname"
	lastName = "Lastname"
	address1 = "Address"
	city = "city"
	zip = "zip"
	phone = "12345"

	cliFirstname = ""
	cliLastName = ""

	firstName = order.Shipping.FirstName
	for _, m := range order.MetaData {
		if m.Key == "_billing_flast_name" {
			lastName = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_rec_address" {
			address1 = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_rec_address_2" {
			city = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_rec_post_code" {
			zip = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_receiver_phone_number" {
			phone = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_sender_Full_Name" {
			cliFirstname = fmt.Sprintf("%v", m.Value)
		}

		if m.Key == "_billing_sender_last_name" {
			cliLastName = fmt.Sprintf("%v", m.Value)
		}
	}

	shipAddress := ShippingAddress{
		Address1:  address1,
		City:      city,
		Country:   "UK",
		FirstName: firstName,
		LastName:  lastName,
		Phone:     phone,
		Zip:       zip,
	}

	if cliFirstname != "" && cliLastName != "" {
		shipAddress.Phone = fmt.Sprintf("From %s %s using SendTo.store", cliFirstname, cliLastName)
	}

	var lineItems []ShopifyLineItem
	for _, l := range order.LineItems {
		qty, _ := l.Quantity.Int64()
		lineItem := ShopifyLineItem{
			Title:    l.Name,
			Quantity: int(qty),
			Price:    l.Total,
		}

		lineItems = append(lineItems, lineItem)
	}

	var shippingLines []ShopifyShippingLine
	if len(order.ShippingLines) > 0 {
		shippingLine := ShopifyShippingLine{
			Price: order.ShippingLines[0].Total,
			Title: order.ShippingLines[0].MethodTitle,
		}
		shippingLines = append(shippingLines, shippingLine)
	}

	customer := Customer{
		FirstName: "",
		Email:     "",
	}

	shopifyOrder := &ShopifyOrder{
		Currency:        "GBP",
		FinancialStatus: "paid",
		Phone:           "+",
		Customer:        customer,
		LineItems:       lineItems,
		ShippingAddress: shipAddress,
		ShippingLines:   shippingLines,
		Note:            fmt.Sprintf("Note_%d", order.Id),
	}

	return shopifyOrder
}
