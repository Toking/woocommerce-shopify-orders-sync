package internal

import "encoding/json"

type (
	WooOrder struct {
		Id              int            `json:"id,omitempty"`
		OrderKey        string         `json:"order_key,omitempty"`
		DateCreated     string         `json:"date_created,omitempty"`
		Status          string         `json:"status,omitempty"`
		Currency        string         `json:"currency,omitempty"`
		ShippingTax     string         `json:"shipping_tax,omitempty"`
		ShippingTotal   string         `json:"shipping_total,omitempty"`
		Total           string         `json:"total,omitempty"`
		DiscountTotal   string         `json:"discount_total,omitempty"`
		PriceIncludeTax bool           `json:"prices_include_tax,omitempty"`
		Billing         Billing        `json:"billing,omitempty"`
		Shipping        Billing        `json:"shipping,omitempty"`
		ShippingLines   []ShippingLine `json:"shipping_lines,omitempty"`
		PaymentMethod   string         `json:"payment_method_title,omitempty"`
		PaymentMethodID string         `json:"payment_method,omitempty"`
		CustomerId      int            `json:"customer_id,omitempty"`
		TransactionId   string         `json:"transaction_id,omitempty"`
		LineItems       []LineItem     `json:"line_items,omitempty"`
		MetaData        []MetaData     `json:"meta_data,omitempty"`
		CustomerNote    string         `json:"customer_note,omitempty"`
	}

	Billing struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Company   string `json:"company"`
		Address1  string `json:"address_1"`
		Address2  string `json:"address_2"`
		PostCode  string `json:"postcode"`
		Country   string `json:"country"`
		Email     string `json:"email"`
		Phone     string `json:"phone"`
		City      string `json:"city"`
		State     string `json:"state"`
		VatNumber string `json:"-"`
	}

	UpdateWooOrder struct {
		Status   string     `json:"status,omitempty"`
		MetaData []MetaData `json:"meta_data,omitempty"`
	}

	ShippingLine struct {
		Id          int    `json:"id"`
		MethodTitle string `json:"method_title"`
		MethodID    string `json:"method_id"`
		Total       string `json:"total"`
		TotalTax    string `json:"total_tax"`
		Taxes       []struct {
			ID       int    `json:"id"`
			Total    string `json:"total"`
			Subtotal string `json:"subtotal"`
		} `json:"taxes"`
	}

	LineItem struct {
		Id          int         `json:"id"`
		Name        string      `json:"name"`
		ProductId   int         `json:"product_id"`
		VariationID int         `json:"variation_id"`
		Quantity    json.Number `json:"quantity"`
		Price       float64     `json:"price"`
		SubTotalTax string      `json:"subtotal_tax"`
		TotalTax    string      `json:"total_tax"`
		Total       string      `json:"total"`
		TaxClass    string      `json:"tax_class,omitempty"`
		SubTotal    string      `json:"subtotal"`
		MetaData    []MetaData  `json:"meta_data,omitempty"`
		Taxes       []struct {
			ID       int    `json:"id"`
			Total    string `json:"total"`
			Subtotal string `json:"subtotal"`
		} `json:"taxes"`
	}

	MetaData struct {
		ID    int         `json:"id,omitempty"`
		Key   string      `json:"key"`
		Value interface{} `json:"value"`
	}
)
