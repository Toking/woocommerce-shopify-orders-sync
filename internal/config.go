package internal

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type (
	Config struct {
		WooUrl    string
		WooKey    string
		WooSecret string
		Shopify   map[string]string
	}
)

func New(path string) (*Config, error) {
	conf := &Config{}
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		return nil, err
	}

	b, err := ioutil.ReadFile(path)
	if err := json.Unmarshal(b, conf); err != nil {
		return nil, err
	}

	return conf, nil
}
