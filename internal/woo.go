package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type (
	WooClient struct {
		Url    string
		Key    string
		Secret string
		Cli    *http.Client
	}
)

func NewWooClient(c *Config) *WooClient {
	return &WooClient{
		c.WooUrl,
		c.WooKey,
		c.WooSecret,
		http.DefaultClient,
	}
}

func (c *WooClient) GetWooOrdersByTimestamp(timestamp string, page, limit int) ([]WooOrder, error) {
	path := c.Url + "/orders"

	q := url.Values{}
	q.Add("consumer_key", c.Key)
	q.Add("consumer_secret", c.Secret)
	q.Add("page", strconv.Itoa(page))
	q.Add("per_page", strconv.Itoa(limit))
	if timestamp != "" {
		q.Add("modified_after", timestamp)
	}

	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, err
	}
	req.URL.RawQuery = q.Encode()

	resp, err := c.Cli.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res []WooOrder
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("get orders request failed with status code %d, body: %s", resp.StatusCode, string(bodyBytes))
	}

	if err := json.NewDecoder(bytes.NewBuffer(bodyBytes)).Decode(&res); err != nil {
		return nil, err
	}

	if len(res) < 1 {
		return nil, nil
	}

	return res, nil
}

func (c *WooClient) UpdateWooOrderWithShopifyID(id string, order UpdateWooOrder) (*WooOrder, error) {
	path := c.Url + fmt.Sprintf("/orders/%s", id)

	q := url.Values{}
	q.Add("consumer_key", c.Key)
	q.Add("consumer_secret", c.Secret)

	jsonBody, err := json.Marshal(order)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPut, path, bytes.NewReader(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.URL.RawQuery = q.Encode()

	resp, err := c.Cli.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	res := &WooOrder{}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("update order request failed with status code %d, body: %s", resp.StatusCode, string(bodyBytes))
	}

	if err := json.NewDecoder(bytes.NewBuffer(bodyBytes)).Decode(&res); err != nil {
		return nil, err
	}

	return res, nil
}
